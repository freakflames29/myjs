// variables
const courselist = document.getElementById("courses-list");
const cart = document.querySelector("#cart-content tbody");
const clearcart = document.getElementById("clear-cart");

// addEventListener
eve();
function eve() {
  courselist.addEventListener("click", collect);
  cart.addEventListener("click", removeit);
  clearcart.addEventListener("click", saaf);
  document.addEventListener("DOMContentLoaded", readfromlocal);
}
// function

function collect(e) {
  e.preventDefault();
  if (e.target.classList.contains("add-to-cart")) {
    let ele = e.target.parentElement.parentElement;
    addthis(ele);
  }
}
// elemnt add function
function addthis(ele) {
  let read = {
    image: ele.querySelector("img").src,
    text: ele.querySelector("h4").textContent,
    price: ele.querySelector(".price span").textContent,
    id: ele.querySelector("a").getAttribute("data-id"),
  };
  app(read);
  saveitlocal(read);
}
// creating table data and putting it in cart
function app(read) {
  let row = document.createElement("tr");
  row.innerHTML = `

                        <td>
                        <img src="${read.image}" width="100" >
                        </td>
                        <td>${read.text}</td>
                        <td>${read.price}</td>
                        <td>
                        <a href="#" data-id="${read.id}" class="remove">X</a>
                        </td>
                        
    `;
  cart.appendChild(row);
}

// remove function
function removeit(e) {
  if (e.target.classList.contains("remove")) {
    e.target.parentElement.parentElement.remove();
  }
  let x = e.target.parentElement.parentElement;
  let data = x.querySelector("a").getAttribute("data-id");
  delbyx(data);
}
// clear button saaf
function saaf() {
  //   cart.innerHTML = "";
  while (cart.firstChild) {
    cart.removeChild(cart.firstChild);
  }
  delfromlocal();
}

// localStorage add function
function saveitlocal(read) {
  let sit = getfromlocal();
  sit.push(read);
  localStorage.setItem("co", JSON.stringify(sit));
}
function getfromlocal() {
  let arr;
  let mango = localStorage.getItem("co");
  if (mango == null) {
    arr = [];
  } else {
    arr = JSON.parse(mango);
  }
  return arr;
}

// read from localStorage function
function readfromlocal() {
  let r = getfromlocal();
  r.forEach(function (read) {
    let rowdie = document.createElement("tr");
    rowdie.innerHTML = `
      
                              <td>
                              <img src="${read.image}" width="100" >
                              </td>
                              <td>${read.text}</td>
                              <td>${read.price}</td>
                              <td>
                              <a href="#" data-id="${read.id}" class="remove">X</a>
                              </td>
                              
          `;
    cart.appendChild(rowdie);
  });
}

// delete from localStorage function by btn
function delfromlocal() {
  localStorage.clear();
}
// delete from localStorage function by X
function delbyx(data) {
  let gfl = getfromlocal();
  let elm = data;
  gfl.forEach(function (idfordelete, index) {
    if (idfordelete.id == elm) {
      gfl.splice(index, 1);
    }
  });
  localStorage.setItem("co", JSON.stringify(gfl));
}
