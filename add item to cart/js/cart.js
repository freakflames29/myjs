// variables
const coursList = document.getElementById("courses-list");
const cc = document.querySelector("#cart-content tbody");
const clearbtn = document.querySelector("#clear-cart");

// console.log("hello");

// addEventListener
eve();
function eve() {
  coursList.addEventListener("click", add);
  cc.addEventListener("click", remcou);
  clearbtn.addEventListener("click", allrem);
  //   loading Element from localStorage
  document.addEventListener("DOMContentLoaded", retrive);
}

// function

function add(e) {
  e.preventDefault();
  if (e.target.classList.contains("add-to-cart")) {
    let read = e.target.parentElement.parentElement;
    // console.log(read);
    cart(read);
  }
}
function cart(read) {
  let obj = {
    images: read.querySelector("img").src,
    text: read.querySelector("h4").textContent,
    price: read.querySelector(".price span").textContent,
    id: read.querySelector("a").getAttribute("data-id"),
  };
  icon(obj);
}
function icon(obj) {
  let row = document.createElement("tr");
  row.innerHTML = `
                    <td>
                    <img src="${obj.images}" width=100>
                    </td>
                    <td>${obj.text}</td>
                    <td>${obj.price}</td>
                    <td>
                    <a class="remove" data-id="${obj.id}" href="#">X</a>
                    </td>



    `;
  cc.appendChild(row);
  savetolocal(obj);
}
// removing courses

function remcou(e) {
  if (e.target.classList.contains("remove")) {
    e.target.parentElement.parentElement.remove();
  }
  let dj = e.target.parentElement.parentElement;
  let mia = dj.querySelector("a").getAttribute("data-id");
  ekdamsaaf(mia);
}

// remove all elemnent from cart
function allrem() {
  //   cc.innerHTML = "";
  while (cc.lastChild) {
    cc.removeChild(cc.lastChild);
  }
  clearfromlocal();
}

// adding courses to localStorage

function savetolocal(course) {
  let c = getfromlocal();
  c.push(course);
  localStorage.setItem("course", JSON.stringify(c));
}
function getfromlocal() {
  let arr;
  let mango = localStorage.getItem("course");
  if (mango === null) {
    arr = [];
  } else {
    arr = JSON.parse(mango);
  }
  return arr;
}

// printing Element from localStorage
function retrive() {
  let idea = getfromlocal();
  idea.forEach(function (obj) {
    let rowdie = document.createElement("tr");
    rowdie.innerHTML = `
                          <td>
                          <img src="${obj.images}" width=100>
                          </td>
                          <td>${obj.text}</td>
                          <td>${obj.price}</td>
                          <td>
                          <a class="remove" data-id="${obj.id}" href="#">X</a>
                          </td>
      
      
      
          `;
    cc.appendChild(rowdie);
  });
}
// clear from localStorage
function clearfromlocal() {
  localStorage.clear();
}

// remove from localStorage
function ekdamsaaf(mia) {
  let sexy = getfromlocal();
  let lobe = mia;
  sexy.forEach(function (m, index) {
    if (m.id == mia) {
      sexy.splice(index, 1);
    }
  });
  localStorage.setItem("course", JSON.stringify(sexy));
}
