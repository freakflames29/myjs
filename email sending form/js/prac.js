// varriables

const sub = document.getElementById("sendBtn"),
  email = document.getElementById("email"),
  subject = document.getElementById("subject"),
  message = document.getElementById("message"),
  form=document.getElementById("email-form"),
  resbtn=document.getElementById("resetBtn");
// addEventListener
eve();
function eve() {
  document.addEventListener("DOMContentLoaded", dis);
  email.addEventListener("blur", val);
  message.addEventListener("blur", val);
  subject.addEventListener("blur", val);
  resbtn.addEventListener("click",res);
  form.addEventListener("submit",mes);
  
}

// function
function dis() {
  //disabling the button
  sub.disabled = true;
}
function val() {
  valdiate(this);
  if (this.type === "email") {
    emailval(this);
  }
  let errors=document.querySelectorAll(".error");
  if(email.value!==""&&message.value!==""&&subject.value!=="")
  {
      if(errors.length===0)
      {
            sub.disabled=false;
      }
  }

}
function valdiate(field) {
  if (field.value.length > 0) {
    field.style.borderBottomColor = "green";
    field.classList.remove("error");
  } else {
    field.style.borderBottomColor = "red";
    field.classList.add("error");
  }
}
function emailval(field) {
  let emailop = field.value;
  if (emailop.indexOf("@") !== -1) {
    field.style.borderBottomColor = "green";
    field.classList.remove("error");
  } else {
    field.style.borderBottomColor = "red";
    field.classList.add("error");
  }
}
// reset
function res()
{
    form.reset();
    location.reload();

}


// gif
function mes(e)
{
    e.preventDefault();
    let spin=document.getElementById("spinner");
    spin.style.display="block";
    let suc=document.createElement("h3");
    suc.textContent="mail delevered";
    suc.classList="mail-del";
    
    setTimeout(function()
    {
        spin.style.display="none";
        document.getElementById("loaders").appendChild(suc);
        setTimeout(function()
        {
            suc.remove();
            form.reset();
            location.reload();
        },3000);
        
    },3000);

}