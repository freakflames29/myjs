// variables
const btnsub = document.getElementById("sendBtn"),
  email = document.getElementById("email"),
  message = document.getElementById("message"),
  subject = document.getElementById("subject"),
  resetbtn = document.getElementById("resetBtn"),
  form = document.getElementById("email-form");

// addEventListener
eve();

function eve() {
  document.addEventListener("DOMContentLoaded", appint);
  email.addEventListener("blur", check);
  message.addEventListener("blur", check);
  subject.addEventListener("blur", check);
  //   resetbtn and submit button
  resetbtn.addEventListener("click", resform);
  form.addEventListener("submit", spinnesr);
}

// function
function appint() {
  btnsub.disabled = true;
}
function check() {
  valfield(this);
  if (this.type === "email") {
    valemail(this);
  }
  let erros = document.querySelectorAll(".error");
  if (email.value !== "" && message.value !== "" && subject.value !== "") {
    if (erros.length === 0) {
      btnsub.disabled = false;
    }
  }
}
function valfield(field) {
  if (field.value.length > 0) {
    field.style.borderBottomColor = "green";
    field.classList.remove("error");
  } else {
    field.style.borderBottomColor = "red";
    field.classList.add("error");
  }
}
// validating email with @ sign
function valemail(field) {
  let emailtext = field.value;
  if (emailtext.indexOf("@") !== -1) {
    field.style.borderBottomColor = "green";
    field.classList.remove("error");
  } else {
    field.style.borderBottomColor = "red";
    field.classList.add("error");
  }
}
// reset form
function resform() {
  form.reset();
}

// spinner display
function spinnesr(e) {
  e.preventDefault();
  let choc = document.getElementById("spinner");
  choc.style.display = "block";
  let mail = document.createElement("img");
  mail.src = "img/mail.gif";
  mail.style.display = "block";
  let tez = document.createElement("h2");
  tez.textContent = "Mail delivered";
  tez.classList = "mail-del";
  setTimeout(function () {
    //   hide gif
    choc.style.display = "none";
    document.querySelector("#loaders").appendChild(tez);
    setTimeout(function () {
      form.reset();
      tez.remove();
    }, 3000);
  }, 3000);
}
